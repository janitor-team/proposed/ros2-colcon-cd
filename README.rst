colcon-cd
=========

A shell function for `colcon-core <https://github.com/colcon/colcon-core>`_ to change the current working directory.
